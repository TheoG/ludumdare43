﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Door : MonoBehaviour {

	private GameObject leftSide;
	private GameObject rightSide;
	private Quaternion leftSideOriginalRotation;
	private Quaternion rightSideOriginalRotation;
	private bool opened;

	void Start() {
		leftSide = transform.GetChild(0).gameObject;	
		rightSide = transform.GetChild(1).gameObject;
		leftSideOriginalRotation = leftSide.transform.rotation;
		rightSideOriginalRotation = rightSide.transform.rotation;
		opened = false;
	}

	private void OnTriggerEnter(Collider other) {
		if ((other.CompareTag("Player") || other.CompareTag("Animal")) && !opened) {
			float rotateSide = 0f;
			rotateSide = GetRotationSide(other.transform) ? 90f : -90f;

			if (rotateSide > 0f && other.CompareTag("Animal")) {
				//Debug.Log("Animal from inside, don't open the door!");
				return;
			}
			//Debug.Log("Opening the door");
			leftSide.transform.Rotate(Vector3.up, rotateSide, Space.World);
			rightSide.transform.Rotate(Vector3.up, -rotateSide, Space.World);
			opened = true;
		}
	}

	private void OnTriggerExit(Collider other) {
		if ((other.CompareTag("Player") || other.CompareTag("Animal"))) {
			//Debug.Log("OnTriggerExit Door");
			if (opened) {
				rightSide.transform.rotation = rightSideOriginalRotation;
				leftSide.transform.rotation = leftSideOriginalRotation;
				opened = false;
			}
		}
	}

	private bool GetRotationSide(Transform other) {
		Vector3 toTarget = (other.transform.position - transform.position).normalized;

		return Vector3.Dot(toTarget, transform.forward) < 0;
	}
}
