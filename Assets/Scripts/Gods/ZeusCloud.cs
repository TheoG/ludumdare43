﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZeusCloud : MonoBehaviour {
	
	public float height = 10f;
	public float fetchSpeed = 10f;
	public float liftStrength = 15f;
	public GameObject sacrificeParticles;

	public GameObject targetAnimal;
	public God targetGod;
	private GameObject[] gods;

	private bool isClose;

	public enum State {
		LookingForTarget,
		GoingToTarget,
		LiftingTarget,
		GrabbedTarget,
		DroppedTarget
	};

	public State state = State.LookingForTarget;
	Coroutine liftCoroutine;

	void Start() {
		gods = GameObject.FindGameObjectsWithTag("God");
		//Debug.Log("GODS " + gods.Length);
	}

	void ChoseTarget() {
		// Take a random god
		targetGod = gods[Random.Range(0, gods.Length)].GetComponent<God>();

		// Take a random animal that the target god wants
		GameObject[] animals = GameObject.FindGameObjectsWithTag("Animal");
		List<Animal> filteredAnimals = new List<Animal>();
		foreach (GameObject ani in animals) {
			if (ani.GetComponent<Animal>().type == targetGod.requestedSacrifice
				&& ani.GetComponent<Animal>().isCarried == false) {
				filteredAnimals.Add(ani.GetComponent<Animal>());
			}
		}
		if (filteredAnimals.Count > 0) {
			targetAnimal = filteredAnimals[Random.Range(0, filteredAnimals.Count)].gameObject;
			state = State.GoingToTarget;
		}
	}

	void GoToTarget() {
		Vector3 animalFlatPos = new Vector3(targetAnimal.transform.position.x, transform.position.y, targetAnimal.transform.position.z);
		Vector3 lerpPosition = Vector3.MoveTowards(transform.position, animalFlatPos, Time.deltaTime * fetchSpeed);
		transform.position = lerpPosition;
		if (Vector3.Distance(animalFlatPos, lerpPosition) < 1f) {
			state = State.LiftingTarget;
			targetAnimal.GetComponent<Animal>().agent.enabled = false;
			targetAnimal.transform.position = transform.position + (Vector3.up * .5f);
			targetAnimal.transform.parent = transform;
		}
	}

	void GoToTargetGod() {
		Vector3 godFlatPos = new Vector3(targetGod.transform.position.x, transform.position.y, targetGod.transform.position.z);
		Vector3 lerpPosition = Vector3.MoveTowards(transform.position, godFlatPos, Time.deltaTime * fetchSpeed);
		transform.position = lerpPosition;
		if (Vector3.Distance(godFlatPos, lerpPosition) < 1f) {
			state = State.DroppedTarget;
		}
	}

	IEnumerator PullTargetUp() {
		while (targetAnimal.transform.localPosition.y < height) {
			targetAnimal.GetComponent<Rigidbody>().AddForce(Vector3.up * liftStrength * Time.fixedDeltaTime, ForceMode.VelocityChange);
			yield return new WaitForFixedUpdate();
		}
		state = State.GrabbedTarget;
		liftCoroutine = null;
		yield break;
	}

	void Sacrifice() {
		targetAnimal.transform.parent = null;
		targetGod.ReceiveSacrifice(targetAnimal.GetComponent<Animal>());
		targetGod = null;
		targetAnimal = null;
		state = State.LookingForTarget;
		if (liftCoroutine != null) {
			StopCoroutine(liftCoroutine);
			liftCoroutine = null;
		}
	}

	void Update() {
		if (state != State.LookingForTarget && (targetAnimal == null || targetGod == null)) {
			state = State.LookingForTarget;
		}
		if (state == State.LookingForTarget) {
			ChoseTarget();
		} else if (state == State.GoingToTarget) {
			if (targetAnimal.GetComponent<Animal>().isCarried) {
				state = State.LookingForTarget;
				return;
			}
			GoToTarget();
		} else if (state == State.LiftingTarget) {
			targetAnimal.GetComponent<Rigidbody>().isKinematic = false;
			targetAnimal.GetComponent<Animal>().isCarried = true;
			targetAnimal.transform.localPosition = new Vector3(0f, targetAnimal.transform.localPosition.y, 0f);
			if (liftCoroutine == null) {
				liftCoroutine = StartCoroutine("PullTargetUp");
			}
		} else if (state == State.GrabbedTarget) {
			targetAnimal.transform.localPosition = new Vector3(0f, targetAnimal.transform.localPosition.y, 0f);
			if (liftCoroutine == null) {
				liftCoroutine = StartCoroutine("PullTargetUp");
			}
			GoToTargetGod();
		} else if (state == State.DroppedTarget) {
			Sacrifice();
		}
	}

	void OnDestroy() {
		if (targetAnimal) {
			targetAnimal.GetComponent<Animal>().rb.useGravity = true;
			targetAnimal.GetComponent<Animal>().agent.enabled = true;
			targetAnimal.GetComponent<Animal>().isCarried = false;
			targetAnimal.transform.parent = null;
			targetAnimal.GetComponent<Animal>().enabled = true;
			targetAnimal.GetComponent<AudioSource>().enabled = true;
		}
	}
}
