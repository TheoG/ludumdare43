﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Aphrodite : God {

	[Header("UI Effects")]
	public Color ecstaticColor;
	public Color angryColor;
	public GameObject colorFilter;
	public float fadeSpeed = 1f;
	public float fertilityBonusRatio;
	public float fertilityPenaltyRatio;

	private bool angry = false;
	private bool ecstatic = false;
	private bool normal = true;
	private Image colorFilterImage;

	protected new void Start() {
		base.Start();
		colorFilterImage = colorFilter.GetComponent<Image>();
	}

	public override void BeAngry() {
		if (angry) return;
		angry = true;
		normal = false;
		colorFilterImage.color = new Color(angryColor.r, angryColor.g, angryColor.b, 0f);
		Animal[] animals = FindObjectsOfType<Animal>();
		foreach (Animal animal in animals) {
			animal.matingCooldown *= fertilityPenaltyRatio;
		}
	}

	public override void BeEcstatic() {
		if (ecstatic) return;
		ecstatic = true;
		normal = false;
		colorFilterImage.color = new Color(ecstaticColor.r, ecstaticColor.g, ecstaticColor.b, 0f);
		Animal[] animals = FindObjectsOfType<Animal>();
		foreach (Animal animal in animals) {
			animal.matingCooldown *= fertilityBonusRatio;
		}
	}

	protected new void Update() {
		base.Update();
		if (angry || ecstatic) {
			Color newColor = colorFilterImage.color;
			if (newColor.a < 0.3f) {
				newColor.a += Time.deltaTime * fadeSpeed;
				colorFilterImage.color = newColor;
			}
		} else if (normal) {
			Color newColor = colorFilterImage.color;
			if (newColor.a > 0.0f) {
				newColor.a -= Time.deltaTime * fadeSpeed;
				colorFilterImage.color = newColor;
			}
		}
	}

	public override void BeNormal() {
		if (normal) return;
		normal = true;
		ecstatic = false;
		angry = false;
		Animal[] animals = FindObjectsOfType<Animal>();
		foreach (Animal animal in animals) {
			animal.matingCooldown = animal.defaultMatingCooldown;
		}
	}
}