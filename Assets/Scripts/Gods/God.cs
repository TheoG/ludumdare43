﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public abstract class God : MonoBehaviour {

	[Header("God GUI")]
	public string godName;
	public Sprite portrait;
	public Sprite angrySpell;
	public Sprite ecstaticSpell;

	[Header("God mood")]
	[Range(0f, 1f)] public float happiness = 0.5f;
	[Range(0f, 1f)] public float angryLevel = 0.1f;
	[Range(0f, 1f)] public float ecstaticLevel = 0.9f;
	public float happinessDecayRate = 0.05f;
	public float happinessDecayCooldown = 5f;
	private float startDecay;

	[Header("God requests")]
	public GameObject[] requestObjects;
	public Animal.AnimalType requestedSacrifice;
	public bool hasRequest;
	public GameObject sacrificeParticles;

	[Header("God audio")]
	public AudioSource audioSource;
	public AudioSource sacrificeAudioSource;
	public AudioClip[] acceptSounds;
	public AudioClip[] curiousSounds;
	public AudioClip[] demandSounds;
	public AudioClip[] denySounds;
	public AudioClip[] sacrificesSound;

	public GameObject tempAudio;

	public GameManager gameManager;

	protected void Start() {
		gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
		RequestSacrifice();
	}

	public void RequestSacrifice() {
		if (hasRequest == false) {
			requestedSacrifice = Generic.GetRandomEnum<Animal.AnimalType>();
			while (requestedSacrifice == Animal.AnimalType.Fish) {
				requestedSacrifice = Generic.GetRandomEnum<Animal.AnimalType>();
			}
			for (int i = 0; i < requestObjects.Length; i++) {
				// SetActive(requestObjects[i], false);
				requestObjects[i].SetActive(false);
			}
			requestObjects[(int)requestedSacrifice].SetActive(true);
			hasRequest = true;
		}
	}

	IEnumerator Sacrifice(Animal sacrifice) {
		//Debug.Log("Start sacrifice");

		audioSource.clip = Generic.GetRandomInArray<AudioClip>(acceptSounds);
		audioSource.pitch = Random.Range(0.9f, 1.1f);
		audioSource.Play();
		
		sacrificeAudioSource.clip = sacrificesSound[0];
		sacrificeAudioSource.pitch = Random.Range(0.9f, 1.1f);
		sacrificeAudioSource.Play();

		float elapsedTime = 0f;
		float endTime = sacrificeAudioSource.clip.length;
		Vector3 startPos = sacrifice.transform.position;
		Vector3 endPos = requestObjects[0].transform.position;
		if (sacrifice.player != null) {
			sacrifice.player.carriedAnimal = null;
			sacrifice.player = null;
		}
		while (elapsedTime < endTime) {
			sacrifice.transform.position = Vector3.Lerp(startPos, endPos, elapsedTime / endTime);
			elapsedTime += Time.deltaTime;
			yield return 0;
		}

		sacrificeAudioSource.clip = sacrificesSound[1];
		sacrificeAudioSource.pitch = Random.Range(0.9f, 1.1f);
		sacrificeAudioSource.Play();

		Instantiate(sacrificeParticles, requestObjects[0].transform.position, Quaternion.identity);

		if (!gameManager.isGameOver) {
			happiness += sacrifice.value;
			gameManager.UpdateScore(sacrifice.value);
			startDecay = Time.time + happinessDecayCooldown;
		}
		Destroy(sacrifice.gameObject);
		hasRequest = false;
		for (int i = 0; i < requestObjects.Length; i++) {
			requestObjects[i].SetActive(false);
		}
		Invoke("RequestSacrifice", 2f);
		yield break;
	}

	public void ReceiveSacrifice(Animal sacrifice) {
		if (sacrifice.type == Animal.AnimalType.Fish || (hasRequest && sacrifice.type == requestedSacrifice)) {
			//Debug.Log("ASD");
			StartCoroutine("Sacrifice", sacrifice);
		} else {
			audioSource.clip = Generic.GetRandomInArray<AudioClip>(denySounds);
			audioSource.pitch = Random.Range(0.9f, 1.1f);
			audioSource.Play();
		}
	}

	protected void Update() {
		if (Time.time >= startDecay) {
			float angryModifier = 1f;
			if (happiness <= angryLevel) {
				angryModifier = 0.5f;
			}
			happiness -= (happinessDecayRate * Time.deltaTime * angryModifier);
		}
		happiness = Mathf.Clamp(happiness, 0f, 1f);
		if (happiness <= angryLevel) {
			BeAngry();
		} else if (happiness >= ecstaticLevel) {
			BeEcstatic();
		} else {
			BeNormal();
		}
	}

	abstract public void BeAngry();
	abstract public void BeNormal();
	abstract public void BeEcstatic();
}
