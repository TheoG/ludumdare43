﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Poseidon : God {

	[Header("Effects")]
	public float slowFactor = 0.8f;
	public float spawnFishRate = 20f;
	public GameObject fishObject;
	public Pen pen;

	private ParticleSystem rainParticles;
	private bool isAngry;
	private Coroutine ecstaticCoroutine;

	protected new void Start() {
		base.Start();
		rainParticles = GameObject.FindGameObjectWithTag("Rain").GetComponent<ParticleSystem>();
		rainParticles.Stop();
		rainParticles.gameObject.GetComponent<AudioSource>().Stop();
	}

	IEnumerator EcstaticCoroutine() {
		Vector3 pos = pen.GetRandSpawnPositionInWater();
		Quaternion randomRotation = Quaternion.Euler(0, Random.Range(0, 360), 0);
		GameObject fish = Instantiate(fishObject, pos, randomRotation);
		fish.GetComponent<Fish>().pen = pen;
		yield return new WaitForSeconds(spawnFishRate);
		ecstaticCoroutine = null;
	}

	public override void BeAngry() {
		if (isAngry == true)
			return;
		rainParticles.Play();
		rainParticles.gameObject.GetComponent<AudioSource>().Play();
		isAngry = true;
		GameObject.FindGameObjectWithTag("Player").GetComponent<Player>().rainSlowFactor = slowFactor;
	}

	public override void BeEcstatic() {
		isAngry = false;
		rainParticles.Stop();
		rainParticles.gameObject.GetComponent<AudioSource>().Stop();
		if (ecstaticCoroutine == null) {
			ecstaticCoroutine = StartCoroutine("EcstaticCoroutine");
		}
	}

	public override void BeNormal() {
		if (isAngry == true) {
			rainParticles.Stop();
			isAngry = false;
			rainParticles.gameObject.GetComponent<AudioSource>().Stop();
			GameObject.FindGameObjectWithTag("Player").GetComponent<Player>().rainSlowFactor = 1f;
		}
	}
}