﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Zeus : God {

	[Header("Zeus specific")]
	public Vector2 timeBetweenZaps;
	public GameObject zapParticles;
	public GameObject zeusCloud;
	public AudioClip zapSound;

	private Coroutine angryCoroutine;
	private Coroutine ecstaticCoroutine;
	private GameObject spawnedCloud;

	IEnumerator AngryCoroutine() {
		float timeUntilZap = Random.Range(timeBetweenZaps.x, timeBetweenZaps.y);
		//Debug.LogFormat("Zeus is going to zap in {0} seconds.", timeUntilZap);
		yield return new WaitForSeconds(timeUntilZap);
		//Debug.Log("Zeus is zapping!");

		GameObject[] animals = GameObject.FindGameObjectsWithTag("Animal");
		if (animals.Length <= 0) {
			angryCoroutine = null;
			yield break;
		}
		GameObject animal = animals[Random.Range(0, animals.Length)];

		Generic.PlayClipAt(zapSound, tempAudio, animal.transform.position);
		yield return new WaitForSeconds(zapSound.length - 0.1f);
		Instantiate(zapParticles, animal.transform.position, animal.transform.rotation);
		Destroy(animal);

		angryCoroutine = null;
		yield break;
	}

	public override void BeAngry() {
		if (angryCoroutine == null) {
			angryCoroutine = StartCoroutine("AngryCoroutine");
		}
	}

	public override void BeEcstatic() {
		if (spawnedCloud == null) {
			spawnedCloud = Instantiate(zeusCloud, transform.position, Quaternion.identity);
		}
	}

	public override void BeNormal() {
		if (angryCoroutine != null) {
			StopCoroutine(angryCoroutine);
		}
		if (ecstaticCoroutine != null) {
			StopCoroutine(ecstaticCoroutine);
		}
		if (spawnedCloud != null) {
			Destroy(spawnedCloud);
			spawnedCloud = null;
		}
	}
}
