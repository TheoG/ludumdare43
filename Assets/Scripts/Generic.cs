using UnityEngine;

public class Generic {
	static public T GetRandomEnum<T>() {
		System.Array A = System.Enum.GetValues(typeof(T));
		T V = (T)A.GetValue(UnityEngine.Random.Range(0,A.Length));
		return V;
	}

	static public T GetRandomInArray<T>(T[] array) {
		if (array.Length > 0) {
			return (array[Random.Range(0, array.Length)]);
		} else {
			return (default(T));
		}
	}

	static public T CopyComponent<T>(T original, GameObject destination) where T : Component {
		System.Type type = original.GetType();
		Component copy = destination.AddComponent(type);
		System.Reflection.FieldInfo[] fields = type.GetFields();
		foreach (System.Reflection.FieldInfo field in fields) {
			field.SetValue(copy, field.GetValue(original));
		}
		return copy as T;
	}

	static public void PlayClipAt(AudioClip clip, GameObject tempAudioSource, Vector3 pos) {
   		GameObject tempGO = GameObject.Instantiate(tempAudioSource, pos, Quaternion.identity);
		tempGO.GetComponent<AudioSource>().clip = clip;
		tempGO.GetComponent<AudioSource>().Play();
		GameObject.Destroy(tempGO, clip.length);
	}
}