﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pen : MonoBehaviour {

	public Animal[] animals;
	public int animalsStartCount;

	public Rect rect;
	public Rect spawnRect;
	public Rect waterSpawnRect;

	// Use this for initialization
	void Start () {
		CalculateLimits();
		SpawnAnimals();
	}

	// Update is called once per frame
	void Update () {

	}

	void CalculateLimits() {
		rect.x = transform.position.x;
		rect.y = transform.position.z;
		spawnRect.x = transform.position.x;
		spawnRect.y = transform.position.z;
		waterSpawnRect.x = transform.position.x + 28f;
		waterSpawnRect.y = transform.position.z + 2f;
		waterSpawnRect.width = 2.7f;
		waterSpawnRect.height = 2.7f;
	}

	private void OnDrawGizmos() {
		DrawLimits();
	}

	void DrawLimits() {
		CalculateLimits();
		Gizmos.color = Color.red;
		Gizmos.DrawSphere(new Vector3(rect.xMin, transform.position.y + 0.5f, rect.yMin), 1f);
		Gizmos.DrawSphere(new Vector3(rect.xMin, transform.position.y + 0.5f, rect.yMax), 1f);
		Gizmos.DrawSphere(new Vector3(rect.xMax, transform.position.y + 0.5f, rect.yMin), 1f);
		Gizmos.DrawSphere(new Vector3(rect.xMax, transform.position.y + 0.5f, rect.yMax), 1f);
		Gizmos.color = Color.green;
		Gizmos.DrawSphere(new Vector3(spawnRect.xMin, transform.position.y + 0.5f, spawnRect.yMin), 1f);
		Gizmos.DrawSphere(new Vector3(spawnRect.xMin, transform.position.y + 0.5f, spawnRect.yMax), 1f);
		Gizmos.DrawSphere(new Vector3(spawnRect.xMax, transform.position.y + 0.5f, spawnRect.yMin), 1f);
		Gizmos.DrawSphere(new Vector3(spawnRect.xMax, transform.position.y + 0.5f, spawnRect.yMax), 1f);
		Gizmos.color = Color.blue;
		Gizmos.DrawSphere(new Vector3(waterSpawnRect.xMin, transform.position.y + 0.5f, waterSpawnRect.yMin), 0.5f);
		Gizmos.DrawSphere(new Vector3(waterSpawnRect.xMin, transform.position.y + 0.5f, waterSpawnRect.yMax), 0.5f);
		Gizmos.DrawSphere(new Vector3(waterSpawnRect.xMax, transform.position.y + 0.5f, waterSpawnRect.yMin), 0.5f);
		Gizmos.DrawSphere(new Vector3(waterSpawnRect.xMax, transform.position.y + 0.5f, waterSpawnRect.yMax), 0.5f);
	}

	void SpawnAnimals() {
		// SpawnAnimal(animals[0]);
		// SpawnAnimal(animals[0]);
		foreach (Animal animal in animals) {
			for (int i = 0; i < animalsStartCount; i++) {
				SpawnAnimal(animal);
			}
		}
	}

	void SpawnAnimal(Animal animal) {
		//Debug.Log("Spawned " + animal.name);
		var randomRotation = Quaternion.Euler(0, Random.Range(0, 360), 0);
		Animal A = Instantiate(animal, GetRandSpawnPositionInPen(), randomRotation);
		A.pen = this;
	}

	public Vector3 GetRandSpawnPositionInPen() {
		return new Vector3(Random.Range(spawnRect.xMin, spawnRect.xMax), transform.position.y + 0.5f, Random.Range(spawnRect.yMin, spawnRect.yMax));
	}

	public Vector3 GetRandPositionInPen() {
		return new Vector3(Random.Range(rect.xMin, rect.xMax), transform.position.y + 0.5f, Random.Range(rect.yMin, rect.yMax));
	}

	public Vector3 GetRandSpawnPositionInWater() {
		return new Vector3(Random.Range(waterSpawnRect.xMin, waterSpawnRect.xMax), transform.position.y - 0.3f, Random.Range(waterSpawnRect.yMin, waterSpawnRect.yMax));
	}
}
