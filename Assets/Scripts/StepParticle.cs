﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StepParticle : MonoBehaviour {

	public GameObject grassParticleSystem;
	public GameObject tileParticleSystem;

	private GameObject grassParticleInstance;
	private ParticleSystem grassParticleSystemInstance;
	private Player player;
	private Rigidbody playerRb;

	void Start () {

		player = FindObjectOfType<Player>();
		grassParticleInstance = Instantiate(grassParticleSystem, player.transform.position, Quaternion.identity);
		grassParticleSystemInstance = grassParticleInstance.GetComponent<ParticleSystem>();
		player = player.GetComponent<Player>();
		playerRb = player.GetComponent<Rigidbody>();
	}

	void Update() {
		var emission = grassParticleSystemInstance.emission;
		emission.enabled = playerRb.velocity.magnitude > 1f;
	}

	void LateUpdate() {
		grassParticleInstance.transform.position = player.transform.position;
	}
}
