﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rain : MonoBehaviour {

	public GameObject player;

	// Use this for initialization
	void Start () {
		player = FindObjectOfType<Player>().gameObject;
	}
	
	// Update is called once per frame
	void Update () {
		Vector3 pos = player.transform.position;
		pos.y = transform.position.y;
		transform.position = pos;
	}
}
