﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
                 
public abstract class Animal : MonoBehaviour {

	public enum AnimalType { Chicken = 0, Sheep = 1, Cow = 2, Fish = 3 };
	public AnimalType type;
	public float value;
	public Color color;
	public GameObject baby;

	[Header("Audio")]
	public AudioSource audioSource;
	public AudioClip[] deathSounds;
	public AudioClip[] grabSounds;
	public AudioClip[] idleSounds;
	public AudioClip[] sexySounds;
	public Vector2 idleSoundCooldown = new Vector2(2f, 10f);
	public GameObject tempAudio;

	[Header("Roaming")]
	public float roamFrequency; // average roam per min
	public float averageRoamDuration; // duration to spend roaming
	float lastRoamTime; // time since last roamStart
	float nextRoamTime; // time til next roam
	public Vector3 roamDestination;
	float roamDuration;

	[Header("Mating")]
	public float matingCooldown;
	public float defaultMatingCooldown;
	public float lookForMateRange;
	[HideInInspector]
	public float lastMatingTime;
	[HideInInspector]
	public bool isMating = false;
	[HideInInspector]
	bool isMatingLeader = false;
	[HideInInspector]
	public Animal mate = null;

	public bool isCarried = false;

	//public static int count = 0;
	public static int max = 6;

	public static Dictionary<Animal.AnimalType, int> count = new Dictionary<AnimalType, int>();

	[HideInInspector]
	public Pen pen;
	[HideInInspector]
	public Player player;
	[HideInInspector]
	public Rigidbody rb;
	[HideInInspector]
	public NavMeshAgent agent;

	protected Animator anim;

	IEnumerator IdleSounds() {
		while (true) {
			yield return new WaitForSeconds(Random.Range(idleSoundCooldown.x, idleSoundCooldown.y));
			audioSource.clip = Generic.GetRandomInArray<AudioClip>(idleSounds);
			audioSource.pitch = Random.Range(0.9f, 1.1f);
			audioSource.Play();
		}
	}

	private void Start() {

		StartCoroutine("IdleSounds");

		agent = GetComponent<NavMeshAgent>();
		rb = GetComponent<Rigidbody>();
		anim = GetComponentInChildren<Animator>();

		//aphrodite = Getcom

		lastRoamTime = Mathf.Infinity;
		SetNextRoamTime();

		lastMatingTime = Time.time;
		defaultMatingCooldown = matingCooldown;
		StartBis();

		if (this.type != AnimalType.Fish) {
			if (!Animal.count.ContainsKey(type)) {
				Animal.count.Add(this.type, 1);
			} else {
				Animal.count[this.type]++;
			}
		}

	}

	protected abstract void StartBis();

	virtual protected void Update() {
		if (this.player) {
			// transform.position = player.gameObject.transform.position + new Vector3(0, 4, 0);
			transform.position = player.carryPosition.position;
			transform.rotation = player.gameObject.transform.rotation;
		} else if (!isCarried) {
			if (Animal.count[this.type] < Animal.max && !mate && (Time.time - lastMatingTime > matingCooldown)) {
				LookForMate();
			}
			if (mate && !isMating && Vector3.Distance(mate.gameObject.transform.position, transform.position) < 1) {
				// Debug.Log("Set isMating to true");
				isMating = true;
				lastMatingTime = Time.time;
				InvokeRepeating("Mate", 1f, 0.5f);
				Invoke("EndMate", 5f);
				if (agent.isOnNavMesh) agent.destination = transform.position;

				// TODO: Disable the agent makes the animal to fall. Need to add a collider? Only a isTrigger sphere collider
				//agent.enabled = false;
			} else if (mate) {
				if (agent.isOnNavMesh) agent.destination = mate.gameObject.transform.position;
			}

			if (!mate) {
				if (Time.time - nextRoamTime > 0) {
					StartRoam();
				}
				if (lastRoamTime - Time.time > roamDuration) {
					if (agent.isOnNavMesh) agent.destination = transform.position;
				}
			}
		}
		UpdateAnimator();
	}

	void UpdateAnimator() {
		if (anim == null || agent == null)
			return;
		anim.SetBool("walk", agent.velocity.magnitude > 0.01f);
		anim.SetBool("carry", player != null);
		anim.SetBool("mate", isMating);
	}


	void Mate() {
		// Debug.Log("Mate");
		//rb.velocity += new Vector3(0, 10, 0);
		rb.AddForce(new Vector3(0f, 10f * Time.deltaTime, 0f), ForceMode.Impulse);

		Vector3 newVelocity = agent.velocity;
		newVelocity.y = rb.velocity.y;

		//agent.velocity = newVelocity;

		//rb.velocity = new Vector3(0f, 1.5f, 0f);
	}

	void EndMate() {
		CancelInvoke();
		isMating = false;
		if (isMatingLeader && mate != null) {
			if (!isCarried) {
				Instantiate(baby, (transform.position + mate.gameObject.transform.position) / 2f + new Vector3(0, 1, 0), Quaternion.identity);
			}
		}
		mate = null;
		agent.enabled = true;
		if (agent.isOnNavMesh) agent.destination = transform.position;
		isMatingLeader = false;
	}

	void LookForMate() {
		// Debug.Log("LookForMate");
		GameObject mate = null;

		Collider[] hitColliders = Physics.OverlapSphere(transform.position, lookForMateRange);
		if (hitColliders.Length == 0) {
			return ;
		}
		int i = 0;
		float dist = Mathf.Infinity;
		while (i < hitColliders.Length) {
			if (hitColliders[i].gameObject.CompareTag("Animal") && hitColliders[i].gameObject != gameObject && hitColliders[i].gameObject.GetComponent<Animal>().type == type && Time.time - hitColliders[i].gameObject.GetComponent<Animal>().lastMatingTime > matingCooldown) {
				float currentDist = Vector3.Distance(hitColliders[i].gameObject.transform.position, transform.position);
				if (currentDist < dist) {
					mate = hitColliders[i].gameObject;
					dist = currentDist;
				}
			}
			i++;
		}
		if (mate) {
			// Debug.Log("Found Mate " + mate.name);
			if (!mate.GetComponent<Animal>().mate) {
				// Debug.Log("Set mating leader");

				isMatingLeader = true;
				this.mate = mate.GetComponent<Animal>();
				this.mate.mate = this;
			}

		}
	}

	protected void StartRoam() {
		if (agent == null || pen == null)
			return;
		if (agent.isOnNavMesh) agent.destination = pen.GetRandPositionInPen();
		SetNextRoamTime();
		roamDuration = averageRoamDuration + Random.Range(-0.5f, 0.5f);
		lastRoamTime = Time.time;
	}

	protected void SetNextRoamTime() {
		nextRoamTime = Random.Range(Mathf.Clamp(60f / roamFrequency - 5, 0, Mathf.Infinity), 60f / roamFrequency + 5) + Time.time;
	}

	public void PickedUp(Player player) {
		isCarried = true;
		this.player = player;
		audioSource.clip = Generic.GetRandomInArray<AudioClip>(grabSounds);
		audioSource.pitch = Random.Range(0.9f, 1.1f);
		audioSource.Play();
		if (isMating) {
			isMating = false;
			isMatingLeader = false;
			if (mate) {
				isMatingLeader = false;
				mate.isMating = false;
				mate.mate = null;
				mate = null;
			}
		}
	}

	virtual public void Dropped() {
		isCarried = false;
		this.player = null;
		audioSource.clip = Generic.GetRandomInArray<AudioClip>(grabSounds);
		audioSource.pitch = Random.Range(0.9f, 1.1f);
		audioSource.Play();
	}

	void RunAway() {

	}

	private void OnDestroy() {
		if (Animal.count.ContainsKey(type)) {
			Animal.count[this.type]--;
		}
		Generic.PlayClipAt(Generic.GetRandomInArray<AudioClip>(deathSounds), tempAudio, transform.position);
	}

	// TODO : WiggleEscape
}
