﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sheep : Animal {
	protected override void StartBis() {
		this.type = AnimalType.Sheep;
		this.value = 0.3f;
	}
}
