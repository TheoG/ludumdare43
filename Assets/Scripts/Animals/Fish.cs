﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fish : Animal {
	
	protected override void StartBis() {
		this.type = AnimalType.Fish;
		this.value = 0.5f;
	}

	protected override void Update() {
		if (this.player) {
			// transform.position = player.gameObject.transform.position + new Vector3(0, 4, 0);
			transform.position = player.carryPosition.position;
			transform.rotation = player.gameObject.transform.rotation;
		}
		UpdateAnimator();
	}

	void UpdateAnimator() {
		anim.SetBool("carry", player != null);
	}

	public override void Dropped() {
		this.transform.position = pen.GetRandSpawnPositionInWater();
		this.transform.rotation = Quaternion.Euler(0, Random.Range(0, 360), 0);
		base.Dropped();
	}
}
