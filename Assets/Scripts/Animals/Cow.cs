﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cow : Animal {
	protected override void StartBis() {
		this.type = AnimalType.Cow;
		this.value = 0.4f;
	}
}
