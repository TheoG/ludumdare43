﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Camera : MonoBehaviour {

	public GameObject target;
	public float zLimit = 40;
	public bool inPen;
	public float closeDistance = 10f;
	public float farDistance = 15f;
	public float angle = 45f;
	public GameObject mainCamera;
	public float transitionSpeed = 2;

	void Start() {
		mainCamera = transform.GetChild(0).gameObject;
		mainCamera.transform.eulerAngles = new Vector3(angle, 0, 0);
		target = GameObject.FindGameObjectWithTag("Player");
	}

	void LateUpdate() {
		mainCamera.transform.eulerAngles = new Vector3(angle, 0, 0);
		inPen = target.transform.position.z < zLimit; 
		Vector3 v = Quaternion.Euler(angle, 0, 0) * new Vector3(0, 0, -1f).normalized * (inPen ? closeDistance : farDistance);

		mainCamera.transform.position = Vector3.Lerp(mainCamera.transform.position, transform.position + v, Time.deltaTime * transitionSpeed);
		transform.position = target.transform.position;
	}
}
