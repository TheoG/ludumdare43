﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GodPanel : MonoBehaviour {
	
	public God god;

	public Image angrySpell;
	public Image ecstaticSpell;
	public Slider happinessSlider;
	public RectTransform fill;

	void Start() {
		angrySpell.sprite = god.angrySpell;
		ecstaticSpell.sprite = god.ecstaticSpell;
	}

	void UpdateSpells() {
		if (god.happiness <= god.angryLevel) {
			angrySpell.enabled = true;
			ecstaticSpell.enabled = false;
		} else if (god.happiness >= god.ecstaticLevel) {
			angrySpell.enabled = false;
			ecstaticSpell.enabled = true;
		} else {
			angrySpell.enabled = false;
			ecstaticSpell.enabled = false;
		}
	}

	void UpdateHapinessSlider() {
		happinessSlider.value = god.happiness;
		if (!fill) return;
		if (happinessSlider.value < god.angryLevel) {
			fill.GetComponent<Image>().color = new Color(1f, 0.4f, 0.4f, 0.7f);
		} else if (happinessSlider.value > god.ecstaticLevel) {
			fill.GetComponent<Image>().color = new Color(0.4f, 1f, 0.4f, 0.7f);
		} else {
			fill.GetComponent<Image>().color = new Color(1f, 1f, 1f, 0.5f);
		}
	}


	void Update() {
		UpdateHapinessSlider();
		UpdateSpells();
	}
}
