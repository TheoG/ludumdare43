﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Temple : MonoBehaviour {

	public float rotationSpeed;
	public GameObject godItemInstance;

	private void Update() {
		godItemInstance.transform.Rotate(Vector3.up, rotationSpeed * Time.deltaTime);
	}
}
