﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour {
	
	public float speed;
	public float rainSlowFactor = 1f;
	public float pickUpRange;
	public Transform carryPosition;

	public bool isOnGrass;
	public bool isNearWater;

	public Animal carriedAnimal;

	public God god;

	public AudioSource audioSource;
	public AudioClip[] dropSounds;
	public AudioClip[] grabSounds;

	private Rigidbody rb;
	private Animator anim;

	private void Start() {
		rb = GetComponent<Rigidbody>();
		anim = this.GetComponentInChildren<Animator>();
		anim.SetBool("walk", false);
		anim.SetBool("carry", false);
		god = null;
		carriedAnimal = null;
		isOnGrass = true;
		isNearWater = false;
	}

	void PickUpAnimal(GameObject animal) {
		if (animal.GetComponent<Animal>().isCarried == false) {
			this.carriedAnimal = animal.GetComponent<Animal>();
			this.carriedAnimal.PickedUp(this);
		}
	}

	void DropAnimal() {
		if (this.carriedAnimal.type == Animal.AnimalType.Fish) {
			if (isNearWater) {
				this.carriedAnimal.Dropped();
				this.carriedAnimal = null;
			}
		} else {
			this.carriedAnimal.Dropped();
			this.carriedAnimal.gameObject.transform.position = transform.position + new Vector3(0, 0, -1);
			this.carriedAnimal = null;
		}
	}

	void AttemptSacrifice() {
		god.ReceiveSacrifice(carriedAnimal);
	}

	public void Update() {
		HandleMovement();
		HandleE();
		UpdateAnimator();
	}

	void UpdateAnimator() {
		anim.SetBool("walk", rb.velocity.magnitude > 0.01f);
		anim.SetBool("carry", carriedAnimal != null);
	}

	void HandleMovement() {
		float horizontalMove = Input.GetAxisRaw("Horizontal");
		float verticalMove = Input.GetAxisRaw("Vertical");
		Vector2 mov = new Vector2(horizontalMove, verticalMove).normalized * speed * rainSlowFactor;
		if (mov.magnitude > 0.01f) {
			transform.forward = Vector3.Lerp(transform.forward, new Vector3(mov.x, transform.forward.y, mov.y), Time.deltaTime);
		}
		rb.velocity = new Vector3(mov.x, rb.velocity.y, mov.y);

	}
	void HandleE() {
		// if (Input.GetKeyDown(KeyCode.E)) {
		if (Input.GetButtonDown("Submit")) {
			if (carriedAnimal) {
				if (this.god) {
					AttemptSacrifice();
				} else {
					DropAnimal();
				}
			} else {
				GameObject animal = GetClosestAnimalInRange();
				if (animal) {
					PickUpAnimal(animal);
				}
			}
		}
	}

	GameObject GetClosestAnimalInRange() {
		GameObject animal = null;

		Collider[] hitColliders = Physics.OverlapSphere(transform.position, pickUpRange);
		if (hitColliders.Length == 0) {
			return null;
		}
        int i = 0;
		float dist = Mathf.Infinity;
		while (i < hitColliders.Length) {
			if (hitColliders[i].gameObject.CompareTag("Animal")) {
				float currentDist = Vector3.Distance(hitColliders[i].gameObject.transform.position, transform.position);
				if (currentDist < dist) {
					animal = hitColliders[i].gameObject;
					dist = currentDist;
				}
			}
			i++;
        }
		return animal;
	}

	private void OnTriggerEnter(Collider other) {
		if (other.CompareTag("God")) {
			god = other.gameObject.GetComponent<God>();	
		} else if (other.CompareTag("Tile")) {
			isOnGrass = false;
		} else if (other.CompareTag("Water")) {
			isNearWater = true;
		}
	}

	private void OnTriggerExit(Collider other) {
		if (other.CompareTag("God")) {
			god = null;
		} else if (other.CompareTag("Tile")) {
			isOnGrass = true;
		} else if (other.CompareTag("Water")) {
			isNearWater = false;
		}
	}
}
	