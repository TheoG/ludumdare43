﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.PostProcessing;
using UnityStandardAssets.ImageEffects;

public class GameManager : MonoBehaviour {
	public God[] gods;

	public GameObject godsArray;
	public GameObject pauseMenu;
	public GameObject gameOverTitleScreen;
	public Text gameOverTitleScreenScoreText;
	public Text scoreText;
	public AudioSource clickSound;

	private bool isPaused = false;
	public bool isGameOver = false;
	private bool isFading = false;
	private GameObject mainCamera;
	private BlurOptimized blurEffect;
	private GameObject gui;

	private PostProcessingBehaviour profile;
	private VignetteModel.Settings settings;

	public float score = 0;
	public float scoreModifier = 100;

	public List<float> increaseDifficultySchedule;
	public float gameTimer = 0f;

	private Coroutine incrementCoroutine = null;

	private void Start() {
		Time.timeScale = 1f;
		mainCamera = FindObjectOfType<UnityEngine.Camera>().gameObject;
		blurEffect = mainCamera.GetComponent<BlurOptimized>();
		gods = FindObjectsOfType<God>();
		gui = GameObject.Find("GUI");

		if (godsArray == null) {
			godsArray = GameObject.Find("GodsArray");
		}
		if (pauseMenu == null) {
			pauseMenu = GameObject.Find("PauseMenu");
		}
		if (gameOverTitleScreen == null) {
			gameOverTitleScreen = GameObject.Find("GameOverMenu");
			gameOverTitleScreenScoreText = gameOverTitleScreen.transform.GetChild(2).GetComponent<Text>();
		}
		if (scoreText == null) {
			GameObject scoreGO = GameObject.Find("Score");
			if (scoreGO) {
				scoreText = scoreGO.GetComponent<Text>();
			}
		}
		pauseMenu.SetActive(false);
		gameOverTitleScreen.SetActive(false);
		StartScoreIncrement();

		profile = FindObjectOfType<PostProcessingBehaviour>();
		settings = profile.profile.vignette.settings;
		settings.intensity = 50f;
		settings.center = new Vector2(0.5f, 0.5f);
		profile.profile.vignette.enabled = true;
		profile.profile.vignette.settings = settings;
		StartCoroutine(FadeIn());
	}

	private void Update() {
		if (!isGameOver) {
			isGameOver = true;
			foreach (God god in gods) {
				if (god.happiness > 0.0001f) {
					isGameOver = false;
				}
			}
		}
		if (Input.GetKeyDown(KeyCode.Escape)) {
			DisplayPauseScreen();
		} else if (isGameOver && !isFading) {
			DisplayGameOverScreen();
		}
		if (!isGameOver) {
			scoreText.text = score.ToString();
			HandleGameTimer();
			HandleDifficulty();
		}
	}

	IEnumerator FadeIn() {
		for (int i = 0; i < 100; i++) {
			settings.intensity -= 0.01f * (100 - i);
			if (settings.intensity < 0f) {
				settings.intensity = 0f;
			}
			profile.profile.vignette.settings = settings;
			yield return new WaitForSeconds(0.001f);
		}
	}

	IEnumerator FadeOut(int level) {
		isFading = true;
		pauseMenu.SetActive(false);
		gameOverTitleScreen.SetActive(false);
		isGameOver = false;
		isPaused = true;
		Time.timeScale = 1f;
		clickSound.Play();
		profile.profile.vignette.enabled = true;
		settings.intensity = 0f;
		profile.profile.vignette.settings = settings;
		for (int i = 0; i < 100; i++) {
			settings.intensity += 0.01f * i;
			profile.profile.vignette.settings = settings;
			yield return new WaitForSeconds(0.001f);
		}
		SceneManager.LoadScene(level);
	}

	void HandleGameTimer() {
		if (!isPaused) {
			gameTimer += Time.deltaTime;
		}
	}

	void HandleDifficulty() {
		if (increaseDifficultySchedule.Count > 0) {
			if (gameTimer > increaseDifficultySchedule[0]) {
				foreach (God god in gods) {
					god.happinessDecayRate += 0.005f;
				}
				increaseDifficultySchedule.RemoveAt(0);
			}
		}
	}


	public void ReloadScene() {
		StartCoroutine(FadeOut(SceneManager.GetActiveScene().buildIndex));
	}

	public void GoToMainMenu() {
		StartCoroutine(FadeOut(0));
	}

	public void DisplayPauseScreen() {
		if (isGameOver) return;
		isPaused = !isPaused;
		if (isPaused) {
			StopScoreIncrement();
			gui.GetComponent<Canvas>().renderMode = RenderMode.ScreenSpaceOverlay;
		} else {
			StartScoreIncrement();
			gui.GetComponent<Canvas>().renderMode = RenderMode.ScreenSpaceCamera;
		}
		Time.timeScale = isPaused ? 0f : 1f;
		blurEffect.enabled = isPaused;
		scoreText.gameObject.SetActive(!isPaused);
		godsArray.SetActive(!isPaused);
		pauseMenu.SetActive(isPaused);
	}

	public void DisplayGameOverScreen() {
		gameOverTitleScreen.SetActive(true);
		gameOverTitleScreenScoreText.text = score.ToString();
		StopScoreIncrement();
		gui.GetComponent<Canvas>().renderMode = RenderMode.ScreenSpaceOverlay;
		blurEffect.enabled = true;
		godsArray.SetActive(false);
		scoreText.gameObject.SetActive(false);
	}

	public void UpdateScore(float value) {
		score += value * scoreModifier;
	}

	void StartScoreIncrement() {
		incrementCoroutine = StartCoroutine("IncrementScore");
	}

	void StopScoreIncrement() {
		if (incrementCoroutine != null) {
			StopCoroutine(incrementCoroutine);	
		}
	}

	IEnumerator IncrementScore() {
		while (true) {
			if (!isGameOver && !isPaused)
				this.score += 1;
			yield return new WaitForSeconds(0.5f);
		}
	} 
}
