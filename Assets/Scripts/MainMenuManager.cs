﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.PostProcessing;

public class MainMenuManager : MonoBehaviour {

	public AudioSource audioSource;

	private PostProcessingBehaviour profile;
	private VignetteModel.Settings settings;

	private void Start() {
		profile = FindObjectOfType<PostProcessingBehaviour>();

		Time.timeScale = 1f;

		GameObject.Find("Cow").GetComponent<Animator>().SetBool("menu", true);
		GameObject.Find("Sheep").GetComponent<Animator>().SetBool("menu", true);
		GameObject.Find("Chicken").GetComponent<Animator>().SetBool("menu", true);
		settings = profile.profile.vignette.settings;
		settings.intensity = 50f;
		settings.center = new Vector2(0.5f, 0.5f);
		profile.profile.vignette.settings = settings;
		StartCoroutine(FadeIn());
	}

	public void LoadScene() {
		audioSource.Play();
		StartCoroutine(FadeOut());
	}

	public void Quit() {
		audioSource.Play();
		Application.Quit();
	}

	IEnumerator FadeIn() {
		for (int i = 0; i < 100; i++) {
			settings.intensity -= 0.01f * (100 - i);
			if (settings.intensity < 0f) {
				settings.intensity = 0f;
			}
			profile.profile.vignette.settings = settings;
			yield return new WaitForSeconds(0.001f);
		}
	}

	IEnumerator FadeOut() {
		for (int i = 0; i < 100; i++) {
			
			settings.intensity += 0.01f * i;
			//Debug.Log("i: " + i);
			profile.profile.vignette.settings = settings;
			yield return new WaitForSeconds(0.001f);
		}
		SceneManager.LoadScene(1);
	}

}
